import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @Input() user: User;
  @Output() isValid = new EventEmitter<boolean>();
  loginForm: FormGroup;

  errorMessage = {
    'email': [
      { type: 'required', message: 'El correo es requerido' },
      { type: 'maxlength', message: 'El correo no debe ser mayor a 30 caracteres' },
      { type: 'pattern', message: 'Porfavor ingrese un correo valido' }
    ],
    'password': [
      { type: 'required', message: 'La contraseña es requerido' },
      { type: 'minlength', message: 'La contraseña debe ser mayor a 6 caracteres' },
    ]
  }

  constructor( public FormBuilder: FormBuilder) { 
    this.loginForm = this.FormBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(30),
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ]))
    });
  }

  ngOnInit() {
    this.loginForm.valueChanges.subscribe(result => {
      this.user.email = result.email;
      this.user.password = result.password;
      this.isValid.emit(!this.loginForm.valid);
  })

  }

    

}
