import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { ToastController } from '@ionic/angular';
import { User } from 'src/app/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLogged: any;

  constructor( public fireAuth: AngularFireAuth, private toastController: ToastController) {
    fireAuth.authState
    .subscribe(user => 
      (this.isLogged = user ));
   }

   async toast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async login(user: User) {
    try {
      return await this.fireAuth.auth.signInWithEmailAndPassword(user.email, user.password);
    } catch (error) {
      if(error.code === 'auth/user-not-found') {
        this.toast('No se encontro una cuenta asociada con el correo');
      } else if(error.code ==='auth/wrong-password') {
        this.toast('La contraseña es incorrecta');
      }
    }
  }

  async register(user: User) {
    try {
      return await this.fireAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
    } catch (error) {
      console.log(error);
      
      if(error.code == 'auth/email-already-in-use') {
        this.toast('Ya existe una cuenta asociada a este correo');
      }
    }
  }
}
