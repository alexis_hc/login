import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';
import { AuthService } from 'src/app/shared/services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user = new User();
  isValid = true;
  view = true;

  constructor(
    private authService: AuthService, 
    private router: Router,) { }

  ngOnInit() {
  }



  async login() {
    const user = await this.authService.login(this.user);
    if (user) {
      this.router.navigateByUrl('/');
    }
  }

  changeView() {
      this.router.navigateByUrl('/register');
  }

}
