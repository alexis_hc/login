import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  user = new User();
  isValid = true;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  async register() {
    const user = await this.authService.register(this.user);
    if (user) {
        this.router.navigateByUrl('/');
    }
  }
}
