// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC1VsHN9l5H437rfJhFt8dBhg0VGChAYhM",
    authDomain: "login-92c0f.firebaseapp.com",
    databaseURL: "https://login-92c0f.firebaseio.com",
    projectId: "login-92c0f",
    storageBucket: "login-92c0f.appspot.com",
    messagingSenderId: "845033530704",
    appId: "1:845033530704:web:14f95433257cf88ce93855"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
